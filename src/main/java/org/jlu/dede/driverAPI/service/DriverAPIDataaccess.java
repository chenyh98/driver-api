package org.jlu.dede.driverAPI.service;

import org.jlu.dede.publicUtlis.model.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Service
@FeignClient(value = "dataaccess",path = "/passenger")
public interface DriverAPIDataaccess {
    @GetMapping("/orders/{id}")
    @ResponseBody
    Order findById(@PathVariable Integer id);
}
