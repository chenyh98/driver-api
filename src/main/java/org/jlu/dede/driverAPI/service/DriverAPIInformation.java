package org.jlu.dede.driverAPI.service;

import org.jlu.dede.publicUtlis.model.Account;
import org.jlu.dede.publicUtlis.model.Car;
import org.jlu.dede.publicUtlis.model.Driver;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Service
@FeignClient(name = "service-information",url = "10.100.0.5:8769")
public interface DriverAPIInformation {

    //输入邮箱点击确定（发送验证码）
    @RequestMapping(value = "/driver/register/send",method = RequestMethod.POST)
    boolean registerSend(@RequestParam(value = "emailAccount")String emailAccount);

    //输入验证码和密码点击确定（验证验证码并输入密码）
    @RequestMapping(value ="/driver/register/verify",method = RequestMethod.POST)
    boolean  registerVerify(@RequestParam(value = "verifyCode") String verifyCode,@RequestParam(value = "password") String password,@RequestParam(value = "emailAccount") String emailAccount);

    //输入邮箱密码点击登录
    @RequestMapping(value ="/driver/logIn",method = RequestMethod.POST)
    ResponseEntity<String> logIn(@RequestParam(value = "emailAccount") String emailAccount, @RequestParam("password") String password);

    //验证司机身份
    @RequestMapping(value = "/driver/verification",method = RequestMethod.POST)
    ResponseEntity<Map<String,Object>> verification(@RequestParam(value = "token")String token);

    //登出
    @RequestMapping(value = "/driver/logOut",method = RequestMethod.POST)
    ResponseEntity<String> logOut(@RequestParam String token);


    // 编辑个人资料
    @RequestMapping(value = "/driver/editUserData/{id}",method = RequestMethod.POST)
    boolean editUserData(@PathVariable Integer id, @RequestBody Driver driver);

    //查询个人账户
    @RequestMapping(value = "/driver/queryDriverAccount", method = RequestMethod.GET)
    Account queryDriverAccount(@RequestParam(value = "id")Integer id);

    //查询个人资料
    @RequestMapping("/driver/queryUserData")
    Driver queryUserData(@RequestParam(value = "id") Integer id);

    //查询车
    @RequestMapping("/driver/queryCarData")
    List<Car> queryCarData(@RequestParam Integer id);

    //增加车
    @RequestMapping("/driver/addCarData/{id}")
    boolean addCarData(@PathVariable Integer id,@RequestBody Car car);

    //删除车
    @RequestMapping(value = "/driver/deleteCarData",method = RequestMethod.DELETE)
    boolean deleteCarData(@RequestParam(value = "id") Integer id);

    //选择车
    @RequestMapping(value = "/driver/chooseCarData/{id}", method = RequestMethod.POST)
    boolean chooseCarData(@PathVariable Integer id,@RequestBody Car car);

    // 司机开始快车或者专车接单工作（0：没有开始工作，1：快车，2：专车）
    @RequestMapping("/driver/startWork")
    boolean startWork(@RequestParam(value = "id") Integer id,@RequestParam(value = "state") int state);
}
