package org.jlu.dede.driverAPI.service;

import org.jlu.dede.publicUtlis.model.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Service
@FeignClient(name = "service-order",url = "10.100.0.1:8768")
public interface DriverAPIOrder {

    //司机接受系统推送当前订单
    @RequestMapping(value = "/order/driver/acceptCurrentOrder",method = RequestMethod.POST)
    boolean acceptCurrentOrder(@RequestBody Order order);

    //司机接受系统推送预约订单
    @RequestMapping(value = "/order/driver/acceptBookingOrder",method = RequestMethod.POST)
    Order acceptBookingOrder(@RequestBody Order order);

    //取消当前订单
    @RequestMapping(value = "/order/driver/cancelCurrentOrder",method = RequestMethod.POST)
    boolean cancelCurrentOrder(@RequestParam (value ="id") Integer id);

    //取消预约订单
    @RequestMapping(value = "/order/driver/cancelBookingOrder",method = RequestMethod.POST)
    boolean cancelBookingOrder(@RequestBody Order order);

    //查看当前订单
    @RequestMapping(value = "/order/driver/queryCurrentOrder",method = RequestMethod.POST)
    Order queryCurrentOrder(@RequestParam (value ="id") Integer id);

    //查看预约订单
    @RequestMapping(value = "/order/driver/queryBookingOrder",method = RequestMethod.POST)
    List<Order> queryBookingOrder(@RequestParam(value ="id")  Integer id);

    //司机开始旅途
    @RequestMapping(value = "/order/driver/startDriving",method = RequestMethod.POST)
    boolean startDriving(@RequestParam(value ="id")  Integer id);

    //司机结束旅途
    @RequestMapping(value = "/order/driver/arriveDestination",method = RequestMethod.POST)
    boolean arriveDestination(@RequestParam("id") Integer id);

    //查询历史订单
    @RequestMapping(value = "/order/driver/queryHistoryOrder",method = RequestMethod.POST)
    List<Order> queryHistoryOrder(@RequestParam(value ="id") Integer id);
}
