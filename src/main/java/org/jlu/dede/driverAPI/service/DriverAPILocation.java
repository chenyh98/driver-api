package org.jlu.dede.driverAPI.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Service
@FeignClient(name = "service-location",url = "10.100.0.6:8770")
public interface DriverAPILocation {

    //更新司机实时位置
    @RequestMapping(value = "/location/updateDriverLocation", method = RequestMethod.POST)
    boolean updateLocationDriver(@RequestParam(value = "x") String x, @RequestParam(value = "y") String y, @RequestParam(value = "id") Integer id);

    //计算行驶距离，推送给司机和乘客
    @RequestMapping(value = "/location/updateDistance", method = RequestMethod.POST)
    boolean updateDistance(@RequestParam(value = "x") String x, @RequestParam(value = "y") String y, @RequestParam(value = "id") Integer id);


    //接单后推送向乘客推送司机位置（订单id）
    @RequestMapping(value = "/location/findYourDriverSite", method = RequestMethod.POST)
    boolean findYourDriverSite(@RequestParam(value = "id") Integer id);
}