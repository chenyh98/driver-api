package org.jlu.dede.driverAPI.service;

import org.jlu.dede.publicUtlis.model.Order;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@Service
@FeignClient(name = "push-model",url = "10.100.0.0:8764")
public interface DriverAPIPush {

    @RequestMapping(value="/driver/pullBookingOrder",method = RequestMethod.POST)
    List<Order> pullBookingOrder(@RequestParam (value ="id") Integer id);

    @RequestMapping(value="/driver/pullCurrentOrder",method = RequestMethod.POST)
    List<Order> pullCurrentOrder(@RequestParam (value ="id") Integer id);
}
