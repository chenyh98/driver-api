package org.jlu.dede.driverAPI.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@Service
@FeignClient(name = "service-deal",url = "10.100.0.4:8767")
public interface DriverAPITransaction {

    //司机提现
    @RequestMapping(value = "/driver/deal/withdraw", method = RequestMethod.POST)
    boolean withdraw(@RequestParam(value = "id") Integer id, @RequestParam(value = "money") BigDecimal money);
}