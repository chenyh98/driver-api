package org.jlu.dede.driverAPI.controller;

import org.jlu.dede.driverAPI.service.*;
import org.jlu.dede.publicUtlis.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.math.BigDecimal;
import java.util.Map;

@RestController
@RequestMapping("/driver")
public class DriverAPIController {

    @Autowired
    DriverAPIDataaccess driverAPIDataaccess;
    @Autowired
    DriverAPIInformation driverAPIInformation;
    @Autowired
    DriverAPIOrder driverAPIOrder;
    @Autowired
    DriverAPITransaction driverAPITransaction;
    @Autowired
    DriverAPILocation driverAPILocation;
    @Autowired
    DriverAPIPush driverAPIPush;



    //司机拉取预约订单
    @RequestMapping(value="/pullBookingOrder",method = RequestMethod.POST)
    public List<Order> pullBookingOrder(@RequestHeader("user_id") String id){
        return driverAPIPush.pullBookingOrder(Integer.parseInt(id));
    }
    //司机拉取当前订单
    @RequestMapping(value="/pullCurrentOrder",method = RequestMethod.POST)
    public List<Order> pullCurrentOrder(@RequestHeader("user_id") String id){
        return driverAPIPush.pullCurrentOrder(Integer.parseInt(id));
    }


    //<!--------------信息维护部分-------------->此模块所有ID都是司机ID
    //输入邮箱点击确定（发送验证码）
    //验证完毕
    @RequestMapping(value = "/register/send",method = RequestMethod.POST)
    @ResponseBody
    public boolean  registerSend(@RequestParam String emailAccount){

        return driverAPIInformation.registerSend(emailAccount);
    }

    //输入验证码和密码点击确定（验证验证码并输入密码）
    //验证完毕(多次发送出现错误)
    @RequestMapping(value = "/register/verify",method = RequestMethod.POST)
    @ResponseBody
    public boolean  registerVerify(@RequestParam(value = "verifyCode") String verifyCode,@RequestParam(value = "password") String password,@RequestParam(value = "emailAccount") String emailAccount){
        return driverAPIInformation.registerVerify(verifyCode,password,emailAccount);
    }

    //输入邮箱密码点击登录
    //验证完毕（多次发送出现connection refused）
    @RequestMapping(value = "/logIn",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> logIn(@RequestParam String emailAccount, @RequestParam String password){
        return  driverAPIInformation.logIn(emailAccount,password);
    }

    //司机验证身份
    //验证完毕
    @RequestMapping(value = "/verification",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Map<String,Object>>verification(@RequestParam String token)
    {
        return driverAPIInformation.verification(token);
    }

    //登出
    //验证完毕
    @RequestMapping(value = "/logOut",method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> logOut(@RequestParam String token)
    {
        return driverAPIInformation.logOut(token);
    }

    // 编辑个人资料
    //验证完毕
    @RequestMapping(value = "/editUserData",method = RequestMethod.POST)
    @ResponseBody

    public boolean editUserData(@RequestHeader("user_id") String id,@RequestBody Driver driver) {
        return driverAPIInformation.editUserData(Integer.parseInt(id),driver);
    }
    //查询个人资料
    //验证完毕
    @RequestMapping("/queryUserData")
    @ResponseBody
    public Driver queryUserData(@RequestHeader("user_id") String id) {
        return driverAPIInformation.queryUserData(Integer.parseInt(id));
    }

    //查询个人账户
    @RequestMapping(value = "/queryDriverAccount", method = RequestMethod.GET)
    @ResponseBody
    public Account queryDriverAccount(@RequestHeader("user_id") String id){

        return driverAPIInformation.queryDriverAccount(Integer.parseInt(id));
    }



    //查询车
    //验证完毕
    @RequestMapping("/queryCarData")
    @ResponseBody
    public List<Car> queryCarData(@RequestHeader("user_id") String id) {
        return driverAPIInformation.queryCarData(Integer.parseInt(id));
    }

    //选择车
    //
    @RequestMapping(value = "/chooseCarData", method = RequestMethod.POST)
    @ResponseBody
    public boolean chooseCarData(@RequestHeader("user_id") String id,@RequestBody Car car){
        return driverAPIInformation.chooseCarData(Integer.parseInt(id),car);
    }

    //增加车
    //验证完毕
    @RequestMapping(value = "/addCarData", method = RequestMethod.POST)
    @ResponseBody
    public boolean addCarData(@RequestHeader("user_id") String id,@RequestBody Car car){
        return driverAPIInformation.addCarData(Integer.parseInt(id),car);
    }


    //删除车
    //验证完毕(有一点问题)
    @RequestMapping(value = "/deleteCarData", method = RequestMethod.DELETE)
    @ResponseBody
    public boolean deleteCarData(@RequestParam Integer id){//此处ID是车的ID
        return driverAPIInformation.deleteCarData(id);
    }

    // 司机开始快车或者专车接单工作（0：没有开始工作，1：快车，2：专车）
    //验证完毕
    @RequestMapping("/startWork")
    @ResponseBody
    public boolean startWork(@RequestHeader("user_id") String id,@RequestParam int state)   {
        return driverAPIInformation.startWork(Integer.parseInt(id),state);
    }


    //    <!-------------订单管理--------------->
    //司机接受系统推送当前订单
    //验证完毕
    @RequestMapping("/acceptCurrentOrder")
    @ResponseBody
    public boolean acceptCurrentOrder(@RequestHeader("user_id") String id,@RequestParam Integer orderID,@RequestParam Integer type)    {
        Order order = new Order();
        Driver driver=new Driver();
        driver.setId(Integer.parseInt(id));
        order.setDriver(driver);
        order.setId(orderID);
        order.setState(1);
        order.setType(type);
        System.out.println("before RETURN1!!!!!");
        return driverAPIOrder.acceptCurrentOrder(order);
    }

    //司机接受系统推送预约订单
    //验证完毕
    @RequestMapping("/acceptBookingOrder")
    @ResponseBody
    public Order acceptBookingOrder(@RequestHeader("user_id") String id,@RequestParam Integer orderID){
        Order order = new Order();
        Driver driver=new Driver();
        driver.setId(Integer.parseInt(id));
        order.setDriver(driver);
        order.setId(orderID);
        order.setState(1);
        order.setType(3);
        return driverAPIOrder.acceptBookingOrder(order);
    }

    //取消当前订单
    //验证完毕
    @RequestMapping("/cancelCurrentOrder")
    @ResponseBody
    public boolean cancelCurrentOrder(@RequestHeader("user_id") String id){
        return driverAPIOrder.cancelCurrentOrder(Integer.parseInt(id));
    }

    //取消预约订单
    //验证完毕
    @RequestMapping("/cancelBookingOrder")
    @ResponseBody
    public boolean cancelBookingOrder(@RequestParam Integer orderID) {
        Order order1 =new Order();
        order1.setId(orderID);
        return driverAPIOrder.cancelBookingOrder(order1);
    }

    //查看当前订单
    //验证完毕
    @RequestMapping("/queryCurrentOrder")
    @ResponseBody
    public Order queryCurrentOrder(@RequestHeader("user_id") String id) {
        return driverAPIOrder.queryCurrentOrder(Integer.parseInt(id));
    }

    //查看预约订单
    //验证完毕
    @RequestMapping("/queryBookingOrder")
    @ResponseBody
    public List<Order> queryBookingOrder(@RequestHeader("user_id") String id) {
        return driverAPIOrder.queryBookingOrder(Integer.parseInt(id));
    }

    //司机开始旅途
    //验证完毕
    @RequestMapping(value = "/startDriving",method = RequestMethod.POST)
    @ResponseBody
    public boolean startDriving(@RequestHeader("user_id") String id){
        return driverAPIOrder.startDriving(Integer.parseInt(id));
    }

    //司机到达目的地
    //验证完毕
    @RequestMapping("/arriveDestination")
    @ResponseBody
    public boolean arriveDestination(@RequestHeader("user_id") String id) {
        return driverAPIOrder.arriveDestination(Integer.parseInt(id));
    }

    //查询历史订单
    //验证完毕
    @RequestMapping("/queryHistoryOrder")
    @ResponseBody
    public List<Order> queryHistoryOrder(@RequestHeader("user_id") String id) {
        return driverAPIOrder.queryHistoryOrder(Integer.parseInt(id));
    }

    //<！--------------交易处理------------------>
    //司机提现
    //验证完毕
    @RequestMapping(value = "/deal/withdraw",method = RequestMethod.POST)
    @ResponseBody
    public boolean withdraw(@RequestHeader("user_id") String id,@RequestParam(value ="money") BigDecimal money)
    {
        return  driverAPITransaction.withdraw(Integer.parseInt(id),money);
    }

    //<!----------------位置更新--------------->
    //更新司机位置信息(10秒钟前端传一次)
    //验证完毕
    @RequestMapping(value = "/updateDriverLocation",method = RequestMethod.POST)
    @ResponseBody
    public boolean updateLocationDriver(@RequestParam(value ="x")String x,@RequestParam(value ="y") String y,@RequestHeader("user_id") String id){
        return driverAPILocation.updateLocationDriver(x,y,Integer.parseInt(id));
    }

    //计算行驶距离，推送给司机和乘客(前端调用)
    //验证完毕
    //id为 order的id
    @RequestMapping(value = "/updateDistance",method = RequestMethod.POST)
    @ResponseBody
    public boolean updateDistance(@RequestParam(value ="x")String x,@RequestParam(value ="y") String y,@RequestParam(value ="id") Integer id){
        return driverAPILocation.updateDistance(x,y,id);
    }


    //接单后向乘客推送司机位置（订单id）
    //订单的id
    @RequestMapping(value = "/findYourDriverSite",method = RequestMethod.POST)
    public boolean findYourDriverSite(Integer id){
        return driverAPILocation.findYourDriverSite(id);
    }
    @PostMapping(value = "/queryOrder")
    Order queryOrder(@RequestParam Integer orderID){
        return driverAPIDataaccess.findById(orderID);
    }




}
